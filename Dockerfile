FROM eclipse-temurin:17.0.3_7-jdk-alpine AS build

COPY mvnw /code/mvnw
COPY .mvn /code/.mvn
COPY pom.xml /code/pom.xml
COPY src /code/src

RUN cd /code && sh mvnw package
RUN cd /code/target && mv *.jar app.jar

FROM eclipse-temurin:17.0.3_7-jre-alpine

COPY --from=build /code/target/app.jar /work/app.jar

ENTRYPOINT ["java", "-jar", "/work/app.jar"]
