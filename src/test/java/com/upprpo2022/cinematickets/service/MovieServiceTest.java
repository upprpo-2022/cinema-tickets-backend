package com.upprpo2022.cinematickets.service;

import com.upprpo2022.cinematickets.domain.Movie;
import com.upprpo2022.cinematickets.dto.MovieListDto;
import com.upprpo2022.cinematickets.repository.MovieRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {MovieService.class})
@ExtendWith(SpringExtension.class)
class MovieServiceTest {
    @MockBean
    private MovieRepository movieRepository;

    @Autowired
    private MovieService movieService;

    @Test
    void testGetMovieList() {
        when(this.movieRepository.findAll((org.springframework.data.domain.Pageable) any()))
                .thenReturn(new PageImpl<>(new ArrayList<>()));
        MovieListDto actualMovieList = this.movieService.getMovieList(10);
        assertEquals(1, actualMovieList.getMaxPages());
        assertEquals(10, actualMovieList.getPageNum());
        assertTrue(actualMovieList.getMovies().isEmpty());
        verify(this.movieRepository).findAll((org.springframework.data.domain.Pageable) any());
    }

    @Test
    void testGetMovieList2() {
        Movie movie = new Movie();
        movie.setDescription("The characteristics of someone or something");
        movie.setId(1);
        movie.setName("id");
        movie.setPreviewObjectKey("id");
        movie.setPreviewUrl("https://example.org/example");

        ArrayList<Movie> movieList = new ArrayList<>();
        movieList.add(movie);
        PageImpl<Movie> pageImpl = new PageImpl<>(movieList);
        when(this.movieRepository.findAll((org.springframework.data.domain.Pageable) any())).thenReturn(pageImpl);
        MovieListDto actualMovieList = this.movieService.getMovieList(10);
        assertEquals(1, actualMovieList.getMaxPages());
        assertEquals(10, actualMovieList.getPageNum());
        List<MovieListDto.MovieDto> movies = actualMovieList.getMovies();
        assertEquals(1, movies.size());
        MovieListDto.MovieDto getResult = movies.get(0);
        assertEquals("The characteristics of someone or something", getResult.getDescription());
        assertEquals("https://example.org/example", getResult.getPreviewUrl());
        assertEquals("id", getResult.getName());
        assertEquals(1, getResult.getId().intValue());
        verify(this.movieRepository).findAll((org.springframework.data.domain.Pageable) any());
    }

    @Test
    void testGetMovieList5() {
        Movie movie = new Movie();
        movie.setDescription("The characteristics of someone or something");
        movie.setId(1);
        movie.setName("id");
        movie.setPreviewObjectKey("id");
        movie.setPreviewUrl("https://example.org/example");

        Movie movie1 = new Movie();
        movie1.setDescription("The characteristics of someone or something");
        movie1.setId(1);
        movie1.setName("id");
        movie1.setPreviewObjectKey("id");
        movie1.setPreviewUrl("https://example.org/example");

        ArrayList<Movie> movieList = new ArrayList<>();
        movieList.add(movie1);
        movieList.add(movie);
        PageImpl<Movie> pageImpl = new PageImpl<>(movieList);
        when(this.movieRepository.findAll((org.springframework.data.domain.Pageable) any())).thenReturn(pageImpl);
        MovieListDto actualMovieList = this.movieService.getMovieList(10);
        assertEquals(1, actualMovieList.getMaxPages());
        assertEquals(10, actualMovieList.getPageNum());
        List<MovieListDto.MovieDto> movies = actualMovieList.getMovies();
        assertEquals(2, movies.size());
        MovieListDto.MovieDto getResult = movies.get(0);
        assertEquals("https://example.org/example", getResult.getPreviewUrl());
        MovieListDto.MovieDto getResult1 = movies.get(1);
        assertEquals("https://example.org/example", getResult1.getPreviewUrl());
        assertEquals("id", getResult1.getName());
        assertEquals(1, getResult1.getId().intValue());
        assertEquals("The characteristics of someone or something", getResult1.getDescription());
        assertEquals("id", getResult.getName());
        assertEquals(1, getResult.getId().intValue());
        assertEquals("The characteristics of someone or something", getResult.getDescription());
        verify(this.movieRepository).findAll((org.springframework.data.domain.Pageable) any());
    }
}

