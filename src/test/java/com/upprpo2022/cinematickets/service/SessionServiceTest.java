package com.upprpo2022.cinematickets.service;

import com.upprpo2022.cinematickets.dto.MovieScheduleDto;
import com.upprpo2022.cinematickets.projections.MovieScheduleProjection;
import com.upprpo2022.cinematickets.repository.SessionRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {SessionService.class})
@ExtendWith(SpringExtension.class)
class SessionServiceTest {
    @MockBean
    private SessionRepository sessionRepository;

    @Autowired
    private SessionService sessionService;

    @Test
    void testGetMovieSchedule() {
        when(this.sessionRepository.findMovieScheduleByMovieIdAndDateTimeBetween(any(), any(),
                any(), any()))
                .thenReturn(new PageImpl<>(new ArrayList<>()));
        MovieScheduleDto actualMovieSchedule = this.sessionService.getMovieSchedule(123, null, null, 10);
        assertEquals(1, actualMovieSchedule.getMaxPages());
        assertTrue(actualMovieSchedule.getTheaters().isEmpty());
        assertEquals(10, actualMovieSchedule.getPageNum());
        verify(this.sessionRepository).findMovieScheduleByMovieIdAndDateTimeBetween(any(), any(),
                any(), any());
    }

    @Test
    void testGetMovieSchedule5() {
        MovieScheduleProjection movieScheduleProjection = mock(MovieScheduleProjection.class);
        when(movieScheduleProjection.getId()).thenReturn(1);
        when(movieScheduleProjection.getTheaterId()).thenReturn(123);
        when(movieScheduleProjection.getTheaterAddress()).thenReturn("42 Main St");
        when(movieScheduleProjection.getTheaterName()).thenReturn("Theater Name");
        when(movieScheduleProjection.getDateTime()).thenReturn(null);

        ArrayList<MovieScheduleProjection> movieScheduleProjectionList = new ArrayList<>();
        movieScheduleProjectionList.add(movieScheduleProjection);
        PageImpl<MovieScheduleProjection> pageImpl = new PageImpl<>(movieScheduleProjectionList);
        when(this.sessionRepository.findMovieScheduleByMovieIdAndDateTimeBetween(any(), any(),
                any(), any())).thenReturn(pageImpl);
        MovieScheduleDto actualMovieSchedule = this.sessionService.getMovieSchedule(123, null, null, 10);
        assertEquals(1, actualMovieSchedule.getMaxPages());
        List<MovieScheduleDto.TheaterDto> theaters = actualMovieSchedule.getTheaters();
        assertEquals(1, theaters.size());
        assertEquals(10, actualMovieSchedule.getPageNum());
        MovieScheduleDto.TheaterDto getResult = theaters.get(0);
        assertEquals("42 Main St", getResult.getAddress());
        List<MovieScheduleDto.TheaterDto.SessionDto> sessions = getResult.getSessions();
        assertEquals(1, sessions.size());
        assertEquals("Theater Name", getResult.getName());
        assertEquals(1, getResult.getId());
        MovieScheduleDto.TheaterDto.SessionDto getResult1 = sessions.get(0);
        assertNull(getResult1.getDateTime());
        assertEquals(1, getResult1.getId());
        verify(this.sessionRepository).findMovieScheduleByMovieIdAndDateTimeBetween(any(), any(),
                any(), any());
        verify(movieScheduleProjection, atLeast(1)).getId();
        verify(movieScheduleProjection).getTheaterId();
        verify(movieScheduleProjection).getTheaterAddress();
        verify(movieScheduleProjection).getTheaterName();
        verify(movieScheduleProjection).getDateTime();
    }

    @Test
    void testGetMovieSchedule6() {
        MovieScheduleProjection movieScheduleProjection = mock(MovieScheduleProjection.class);
        when(movieScheduleProjection.getId()).thenReturn(1);
        when(movieScheduleProjection.getTheaterId()).thenReturn(123);
        when(movieScheduleProjection.getTheaterAddress()).thenReturn("42 Main St");
        when(movieScheduleProjection.getTheaterName()).thenReturn("Theater Name");
        when(movieScheduleProjection.getDateTime()).thenReturn(null);
        MovieScheduleProjection movieScheduleProjection1 = mock(MovieScheduleProjection.class);
        when(movieScheduleProjection1.getId()).thenReturn(1);
        when(movieScheduleProjection1.getTheaterId()).thenReturn(123);
        when(movieScheduleProjection1.getTheaterAddress()).thenReturn("42 Main St");
        when(movieScheduleProjection1.getTheaterName()).thenReturn("Theater Name");
        when(movieScheduleProjection1.getDateTime()).thenReturn(null);

        ArrayList<MovieScheduleProjection> movieScheduleProjectionList = new ArrayList<>();
        movieScheduleProjectionList.add(movieScheduleProjection1);
        movieScheduleProjectionList.add(movieScheduleProjection);
        PageImpl<MovieScheduleProjection> pageImpl = new PageImpl<>(movieScheduleProjectionList);
        when(this.sessionRepository.findMovieScheduleByMovieIdAndDateTimeBetween(any(), any(),
                any(), any())).thenReturn(pageImpl);
        MovieScheduleDto actualMovieSchedule = this.sessionService.getMovieSchedule(123, null, null, 10);
        assertEquals(1, actualMovieSchedule.getMaxPages());
        List<MovieScheduleDto.TheaterDto> theaters = actualMovieSchedule.getTheaters();
        assertEquals(1, theaters.size());
        assertEquals(10, actualMovieSchedule.getPageNum());
        MovieScheduleDto.TheaterDto getResult = theaters.get(0);
        assertEquals("42 Main St", getResult.getAddress());
        List<MovieScheduleDto.TheaterDto.SessionDto> sessions = getResult.getSessions();
        assertEquals(2, sessions.size());
        assertEquals("Theater Name", getResult.getName());
        assertEquals(1, getResult.getId());
        MovieScheduleDto.TheaterDto.SessionDto getResult1 = sessions.get(0);
        assertEquals(1, getResult1.getId());
        MovieScheduleDto.TheaterDto.SessionDto getResult2 = sessions.get(1);
        assertEquals(1, getResult2.getId());
        assertNull(getResult2.getDateTime());
        assertNull(getResult1.getDateTime());
        verify(this.sessionRepository).findMovieScheduleByMovieIdAndDateTimeBetween(any(), any(),
                any(), any());
        verify(movieScheduleProjection1, atLeast(1)).getId();
        verify(movieScheduleProjection1).getTheaterId();
        verify(movieScheduleProjection1).getTheaterAddress();
        verify(movieScheduleProjection1).getTheaterName();
        verify(movieScheduleProjection1).getDateTime();
        verify(movieScheduleProjection).getId();
        verify(movieScheduleProjection).getTheaterId();
        verify(movieScheduleProjection).getDateTime();
    }

    @Test
    void testGetMovieSchedule7() {
        MovieScheduleProjection movieScheduleProjection = mock(MovieScheduleProjection.class);
        when(movieScheduleProjection.getId()).thenReturn(1);
        when(movieScheduleProjection.getTheaterId()).thenReturn(123);
        when(movieScheduleProjection.getTheaterAddress()).thenReturn("42 Main St");
        when(movieScheduleProjection.getTheaterName()).thenReturn("Theater Name");
        when(movieScheduleProjection.getDateTime()).thenReturn(null);
        MovieScheduleProjection movieScheduleProjection1 = mock(MovieScheduleProjection.class);
        when(movieScheduleProjection1.getId()).thenReturn(1);
        when(movieScheduleProjection1.getTheaterId()).thenReturn(1);
        when(movieScheduleProjection1.getTheaterAddress()).thenReturn("42 Main St");
        when(movieScheduleProjection1.getTheaterName()).thenReturn("Theater Name");
        when(movieScheduleProjection1.getDateTime()).thenReturn(null);

        ArrayList<MovieScheduleProjection> movieScheduleProjectionList = new ArrayList<>();
        movieScheduleProjectionList.add(movieScheduleProjection1);
        movieScheduleProjectionList.add(movieScheduleProjection);
        PageImpl<MovieScheduleProjection> pageImpl = new PageImpl<>(movieScheduleProjectionList);
        when(this.sessionRepository.findMovieScheduleByMovieIdAndDateTimeBetween(any(), any(),
                any(), any())).thenReturn(pageImpl);
        MovieScheduleDto actualMovieSchedule = this.sessionService.getMovieSchedule(123, null, null, 10);
        assertEquals(1, actualMovieSchedule.getMaxPages());
        List<MovieScheduleDto.TheaterDto> theaters = actualMovieSchedule.getTheaters();
        assertEquals(2, theaters.size());
        assertEquals(10, actualMovieSchedule.getPageNum());
        MovieScheduleDto.TheaterDto getResult = theaters.get(1);
        List<MovieScheduleDto.TheaterDto.SessionDto> sessions = getResult.getSessions();
        assertEquals(1, sessions.size());
        MovieScheduleDto.TheaterDto getResult1 = theaters.get(0);
        List<MovieScheduleDto.TheaterDto.SessionDto> sessions1 = getResult1.getSessions();
        assertEquals(1, sessions1.size());
        assertEquals("42 Main St", getResult.getAddress());
        assertEquals("Theater Name", getResult.getName());
        assertEquals("Theater Name", getResult1.getName());
        assertEquals(1, getResult1.getId());
        assertEquals("42 Main St", getResult1.getAddress());
        assertEquals(1, getResult.getId());
        MovieScheduleDto.TheaterDto.SessionDto getResult2 = sessions.get(0);
        assertEquals(1, getResult2.getId());
        MovieScheduleDto.TheaterDto.SessionDto getResult3 = sessions1.get(0);
        assertEquals(1, getResult3.getId());
        assertNull(getResult3.getDateTime());
        assertNull(getResult2.getDateTime());
        verify(this.sessionRepository).findMovieScheduleByMovieIdAndDateTimeBetween(any(), any(),
                any(), any());
        verify(movieScheduleProjection1, atLeast(1)).getId();
        verify(movieScheduleProjection1).getTheaterId();
        verify(movieScheduleProjection1).getTheaterAddress();
        verify(movieScheduleProjection1).getTheaterName();
        verify(movieScheduleProjection1).getDateTime();
        verify(movieScheduleProjection, atLeast(1)).getId();
        verify(movieScheduleProjection).getTheaterId();
        verify(movieScheduleProjection).getTheaterAddress();
        verify(movieScheduleProjection).getTheaterName();
        verify(movieScheduleProjection).getDateTime();
    }
}

