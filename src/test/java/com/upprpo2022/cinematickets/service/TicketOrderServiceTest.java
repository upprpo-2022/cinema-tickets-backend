package com.upprpo2022.cinematickets.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.upprpo2022.cinematickets.domain.Auditorium;
import com.upprpo2022.cinematickets.domain.Movie;
import com.upprpo2022.cinematickets.domain.SeatLayout;
import com.upprpo2022.cinematickets.domain.SeatLayoutStatus;
import com.upprpo2022.cinematickets.domain.Session;
import com.upprpo2022.cinematickets.domain.Theater;
import com.upprpo2022.cinematickets.dto.TicketOrderDto;
import com.upprpo2022.cinematickets.exception.ApiResponseStatusException;
import com.upprpo2022.cinematickets.exception.ErrorString;
import com.upprpo2022.cinematickets.repository.SessionRepository;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {TicketOrderService.class})
@ExtendWith(SpringExtension.class)
class TicketOrderServiceTest {
    @MockBean
    private SessionRepository sessionRepository;

    @Autowired
    private TicketOrderService ticketOrderService;

    @Test
    void testHandleTicketOrder() {
        SeatLayout seatLayout = new SeatLayout();
        ArrayList<SeatLayout.Seat> seatList = new ArrayList<>();
        seatLayout.setSeats(seatList);

        Theater theater = new Theater();
        theater.setAddress("42 Main St");
        theater.setId(1);
        theater.setName("Name");

        Auditorium auditorium = new Auditorium();
        auditorium.setId(1);
        auditorium.setSeats(seatLayout);
        auditorium.setTheater(theater);
        auditorium.setTheaterId(123);

        Movie movie = new Movie();
        movie.setDescription("The characteristics of someone or something");
        movie.setId(1);
        movie.setName("Name");
        movie.setPreviewObjectKey("Preview Object Key");
        movie.setPreviewUrl("https://example.org/example");

        SeatLayoutStatus seatLayoutStatus = new SeatLayoutStatus();
        seatLayoutStatus.setSeats(new ArrayList<>());

        Theater theater1 = new Theater();
        theater1.setAddress("42 Main St");
        theater1.setId(1);
        theater1.setName("Name");

        Session session = new Session();
        session.setAuditorium(auditorium);
        session.setAuditoriumId(123);
        session.setDateTime(null);
        session.setId(1);
        session.setMovie(movie);
        session.setMovieId(123);
        session.setSeats(seatLayoutStatus);
        session.setTheater(theater1);
        session.setTheaterId(123);
        Optional<Session> ofResult = Optional.of(session);

        SeatLayout seatLayout1 = new SeatLayout();
        seatLayout1.setSeats(new ArrayList<>());

        Theater theater2 = new Theater();
        theater2.setAddress("42 Main St");
        theater2.setId(1);
        theater2.setName("Name");

        Auditorium auditorium1 = new Auditorium();
        auditorium1.setId(1);
        auditorium1.setSeats(seatLayout1);
        auditorium1.setTheater(theater2);
        auditorium1.setTheaterId(123);

        Movie movie1 = new Movie();
        movie1.setDescription("The characteristics of someone or something");
        movie1.setId(1);
        movie1.setName("Name");
        movie1.setPreviewObjectKey("Preview Object Key");
        movie1.setPreviewUrl("https://example.org/example");

        SeatLayoutStatus seatLayoutStatus1 = new SeatLayoutStatus();
        seatLayoutStatus1.setSeats(new ArrayList<>());

        Theater theater3 = new Theater();
        theater3.setAddress("42 Main St");
        theater3.setId(1);
        theater3.setName("Name");

        Session session1 = new Session();
        session1.setAuditorium(auditorium1);
        session1.setAuditoriumId(123);
        session1.setDateTime(null);
        session1.setId(1);
        session1.setMovie(movie1);
        session1.setMovieId(123);
        session1.setSeats(seatLayoutStatus1);
        session1.setTheater(theater3);
        session1.setTheaterId(123);
        when(this.sessionRepository.save(any())).thenReturn(session1);
        when(this.sessionRepository.findById(any())).thenReturn(ofResult);

        TicketOrderDto ticketOrderDto = new TicketOrderDto();
        ticketOrderDto.setSeats(new ArrayList<>());
        ticketOrderDto.setSessionId(123);
        this.ticketOrderService.handleTicketOrder(ticketOrderDto);
        verify(this.sessionRepository).save(any());
        verify(this.sessionRepository).findById(any());
        assertEquals(seatList, ticketOrderDto.getSeats());
        assertEquals(123, ticketOrderDto.getSessionId());
    }

    @Test
    void testHandleTicketOrder2() {
        SeatLayout seatLayout = new SeatLayout();
        seatLayout.setSeats(new ArrayList<>());

        Theater theater = new Theater();
        theater.setAddress("42 Main St");
        theater.setId(1);
        theater.setName("Name");

        Auditorium auditorium = new Auditorium();
        auditorium.setId(1);
        auditorium.setSeats(seatLayout);
        auditorium.setTheater(theater);
        auditorium.setTheaterId(123);

        Movie movie = new Movie();
        movie.setDescription("The characteristics of someone or something");
        movie.setId(1);
        movie.setName("Name");
        movie.setPreviewObjectKey("Preview Object Key");
        movie.setPreviewUrl("https://example.org/example");

        SeatLayoutStatus seatLayoutStatus = new SeatLayoutStatus();
        seatLayoutStatus.setSeats(new ArrayList<>());

        Theater theater1 = new Theater();
        theater1.setAddress("42 Main St");
        theater1.setId(1);
        theater1.setName("Name");

        Session session = new Session();
        session.setAuditorium(auditorium);
        session.setAuditoriumId(123);
        session.setDateTime(null);
        session.setId(1);
        session.setMovie(movie);
        session.setMovieId(123);
        session.setSeats(seatLayoutStatus);
        session.setTheater(theater1);
        session.setTheaterId(123);
        Optional<Session> ofResult = Optional.of(session);
        when(this.sessionRepository.save(any()))
                .thenThrow(new ApiResponseStatusException(HttpStatus.CONTINUE, ErrorString.UNEXPECTED_ERROR));
        when(this.sessionRepository.findById(any())).thenReturn(ofResult);

        TicketOrderDto ticketOrderDto = new TicketOrderDto();
        ticketOrderDto.setSeats(new ArrayList<>());
        ticketOrderDto.setSessionId(123);
        assertThrows(ApiResponseStatusException.class, () -> this.ticketOrderService.handleTicketOrder(ticketOrderDto));
        verify(this.sessionRepository).save(any());
        verify(this.sessionRepository).findById(any());
    }

    @Test
    void testHandleTicketOrder3() {
        SeatLayout seatLayout = new SeatLayout();
        seatLayout.setSeats(new ArrayList<>());

        Theater theater = new Theater();
        theater.setAddress("42 Main St");
        theater.setId(1);
        theater.setName("Name");

        Auditorium auditorium = new Auditorium();
        auditorium.setId(1);
        auditorium.setSeats(seatLayout);
        auditorium.setTheater(theater);
        auditorium.setTheaterId(123);

        Movie movie = new Movie();
        movie.setDescription("The characteristics of someone or something");
        movie.setId(1);
        movie.setName("Name");
        movie.setPreviewObjectKey("Preview Object Key");
        movie.setPreviewUrl("https://example.org/example");

        SeatLayoutStatus seatLayoutStatus = new SeatLayoutStatus();
        seatLayoutStatus.setSeats(new ArrayList<>());

        Theater theater1 = new Theater();
        theater1.setAddress("42 Main St");
        theater1.setId(1);
        theater1.setName("Name");

        Session session = new Session();
        session.setAuditorium(auditorium);
        session.setAuditoriumId(123);
        session.setDateTime(null);
        session.setId(1);
        session.setMovie(movie);
        session.setMovieId(123);
        session.setSeats(seatLayoutStatus);
        session.setTheater(theater1);
        session.setTheaterId(123);
        when(this.sessionRepository.save(any())).thenReturn(session);
        when(this.sessionRepository.findById(any())).thenReturn(Optional.empty());

        TicketOrderDto ticketOrderDto = new TicketOrderDto();
        ticketOrderDto.setSeats(new ArrayList<>());
        ticketOrderDto.setSessionId(123);
        assertThrows(ApiResponseStatusException.class, () -> this.ticketOrderService.handleTicketOrder(ticketOrderDto));
        verify(this.sessionRepository).findById(any());
    }

    @Test
    void testHandleTicketOrder4() {
        SeatLayout seatLayout = new SeatLayout();
        seatLayout.setSeats(new ArrayList<>());

        Theater theater = new Theater();
        theater.setAddress("42 Main St");
        theater.setId(1);
        theater.setName("Name");

        Auditorium auditorium = new Auditorium();
        auditorium.setId(1);
        auditorium.setSeats(seatLayout);
        auditorium.setTheater(theater);
        auditorium.setTheaterId(123);

        Movie movie = new Movie();
        movie.setDescription("The characteristics of someone or something");
        movie.setId(1);
        movie.setName("Name");
        movie.setPreviewObjectKey("Preview Object Key");
        movie.setPreviewUrl("https://example.org/example");

        SeatLayoutStatus seatLayoutStatus = new SeatLayoutStatus();
        seatLayoutStatus.setSeats(new ArrayList<>());

        Theater theater1 = new Theater();
        theater1.setAddress("42 Main St");
        theater1.setId(1);
        theater1.setName("Name");

        Session session = new Session();
        session.setAuditorium(auditorium);
        session.setAuditoriumId(123);
        session.setDateTime(null);
        session.setId(1);
        session.setMovie(movie);
        session.setMovieId(123);
        session.setSeats(seatLayoutStatus);
        session.setTheater(theater1);
        session.setTheaterId(123);
        Optional<Session> ofResult = Optional.of(session);

        SeatLayout seatLayout1 = new SeatLayout();
        seatLayout1.setSeats(new ArrayList<>());

        Theater theater2 = new Theater();
        theater2.setAddress("42 Main St");
        theater2.setId(1);
        theater2.setName("Name");

        Auditorium auditorium1 = new Auditorium();
        auditorium1.setId(1);
        auditorium1.setSeats(seatLayout1);
        auditorium1.setTheater(theater2);
        auditorium1.setTheaterId(123);

        Movie movie1 = new Movie();
        movie1.setDescription("The characteristics of someone or something");
        movie1.setId(1);
        movie1.setName("Name");
        movie1.setPreviewObjectKey("Preview Object Key");
        movie1.setPreviewUrl("https://example.org/example");

        SeatLayoutStatus seatLayoutStatus1 = new SeatLayoutStatus();
        seatLayoutStatus1.setSeats(new ArrayList<>());

        Theater theater3 = new Theater();
        theater3.setAddress("42 Main St");
        theater3.setId(1);
        theater3.setName("Name");

        Session session1 = new Session();
        session1.setAuditorium(auditorium1);
        session1.setAuditoriumId(123);
        session1.setDateTime(null);
        session1.setId(1);
        session1.setMovie(movie1);
        session1.setMovieId(123);
        session1.setSeats(seatLayoutStatus1);
        session1.setTheater(theater3);
        session1.setTheaterId(123);
        when(this.sessionRepository.save(any())).thenReturn(session1);
        when(this.sessionRepository.findById(any())).thenReturn(ofResult);

        TicketOrderDto.Seat seat = new TicketOrderDto.Seat();
        seat.setPrice(BigDecimal.valueOf(42L));
        seat.setX(2);
        seat.setY(3);

        ArrayList<TicketOrderDto.Seat> seatList = new ArrayList<>();
        seatList.add(seat);

        TicketOrderDto ticketOrderDto = new TicketOrderDto();
        ticketOrderDto.setSeats(seatList);
        ticketOrderDto.setSessionId(123);
        assertThrows(ApiResponseStatusException.class, () -> this.ticketOrderService.handleTicketOrder(ticketOrderDto));
        verify(this.sessionRepository).findById(any());
    }
}

