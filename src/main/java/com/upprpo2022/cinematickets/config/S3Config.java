package com.upprpo2022.cinematickets.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.services.s3.S3Client;

import java.net.URI;

@Configuration
public class S3Config {
    @Bean("moviePreviewBucketName")
    public String moviePreviewBucketName() {
        return "upprpo2022cinematickets-movie-preview-3de7mo6bky";
    }

    @Bean
    public S3Client s3Client() {
        return S3Client.builder()
                .endpointOverride(URI.create("https://storage.yandexcloud.net"))
                .build();
    }
}
