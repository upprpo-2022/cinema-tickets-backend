package com.upprpo2022.cinematickets.projections;

import java.time.OffsetDateTime;

public interface MovieScheduleProjection {
    Integer getId();
    OffsetDateTime getDateTime();

    Integer getTheaterId();
    String getTheaterName();
    String getTheaterAddress();
}
