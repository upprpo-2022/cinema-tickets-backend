package com.upprpo2022.cinematickets.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class MovieDto {
    @JsonProperty("id") private Integer id;
    @JsonProperty("name") private String name;
    @JsonProperty("description") private String description;
    @JsonProperty("previewUrl") private String previewUrl;
}
