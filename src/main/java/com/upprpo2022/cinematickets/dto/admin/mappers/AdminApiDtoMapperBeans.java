package com.upprpo2022.cinematickets.dto.admin.mappers;

import com.upprpo2022.cinematickets.domain.*;
import com.upprpo2022.cinematickets.dto.admin.*;
import org.mapstruct.factory.Mappers;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AdminApiDtoMapperBeans {
    @Bean
    public AdminApiDtoMapper<Auditorium, AdminAuditoriumDto> auditorium() {
        return Mappers.getMapper(AdminAuditoriumDtoMapper.class);
    }

    @Bean
    public AdminApiDtoMapper<Movie, AdminMovieDto> movie() {
        return Mappers.getMapper(AdminMovieDtoMapper.class);
    }

    @Bean
    public AdminApiDtoMapper<Session, AdminSessionDto> session() {
        return Mappers.getMapper(AdminSessionDtoMapper.class);
    }

    @Bean
    public AdminApiDtoMapper<Theater, AdminTheaterDto> theater() {
        return Mappers.getMapper(AdminTheaterDtoMapper.class);
    }

    @Bean
    public AdminApiDtoMapper<AppUser, AdminUserDto> appUser() {
        return Mappers.getMapper(AdminUserDtoMapper.class);
    }
}
