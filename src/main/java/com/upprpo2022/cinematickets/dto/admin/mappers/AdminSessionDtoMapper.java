package com.upprpo2022.cinematickets.dto.admin.mappers;

import com.upprpo2022.cinematickets.domain.Session;
import com.upprpo2022.cinematickets.dto.admin.AdminSessionDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface AdminSessionDtoMapper extends AdminApiDtoMapper<Session, AdminSessionDto> {
    @Mapping(target = "seats", source = "seats.seats")
    AdminSessionDto entityToDto(Session entity);

    @Mapping(target = "seats.seats", source = "seats")
    Session dtoToEntity(AdminSessionDto dto);
}
