package com.upprpo2022.cinematickets.dto.admin.mappers;

import com.upprpo2022.cinematickets.domain.Theater;
import com.upprpo2022.cinematickets.dto.admin.AdminTheaterDto;
import org.mapstruct.Mapper;

@Mapper
public interface AdminTheaterDtoMapper extends AdminApiDtoMapper<Theater, AdminTheaterDto> {
    AdminTheaterDto entityToDto(Theater entity);
    Theater dtoToEntity(AdminTheaterDto dto);
}
