package com.upprpo2022.cinematickets.dto.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class AdminUserDto {
    @JsonProperty("id") private Integer id;
    @JsonProperty("username") private String username;
    @JsonProperty("password") private String password;
    @JsonProperty("role") private String role;
}
