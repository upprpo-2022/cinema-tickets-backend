package com.upprpo2022.cinematickets.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ChangePasswordDto {
    @JsonProperty("username") private String username;
    @JsonProperty("oldPassword") private String oldPassword;
    @JsonProperty("newPassword") private String newPassword;
}
