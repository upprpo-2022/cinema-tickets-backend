package com.upprpo2022.cinematickets.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
public class TicketOrderDto {
    @JsonProperty("session") private int sessionId;
    @JsonProperty("seats") private List<Seat> seats;

    @Getter
    @Setter
    public static class Seat {
        @JsonProperty("x") private Integer x;
        @JsonProperty("y") private Integer y;
        @JsonProperty("price") private BigDecimal price;
    }
}
