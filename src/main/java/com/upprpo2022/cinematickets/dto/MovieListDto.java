package com.upprpo2022.cinematickets.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class MovieListDto {
    @JsonProperty("maxPages") private int maxPages;
    @JsonProperty("pageNum") private int pageNum;
    @JsonProperty("movies") private List<MovieDto> movies;

    @Getter
    @Setter
    @AllArgsConstructor
    public static class MovieDto {
        @JsonProperty("id") private Integer id;
        @JsonProperty("name") private String name;
        @JsonProperty("description") private String description;
        @JsonProperty("previewUrl") private String previewUrl;
    }
}
