package com.upprpo2022.cinematickets.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.OffsetDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class MovieScheduleDto {
    @JsonProperty("maxPages") private int maxPages;
    @JsonProperty("pageNum") private int pageNum;
    @JsonProperty("theaters") private List<TheaterDto> theaters;

    @Getter
    @Setter
    @AllArgsConstructor
    public static class TheaterDto {
        @JsonProperty("id") private int id;
        @JsonProperty("name") private String name;
        @JsonProperty("address") private String address;
        @JsonProperty("sessions") private List<SessionDto> sessions;

        @Getter
        @Setter
        @AllArgsConstructor
        public static class SessionDto {
            @JsonProperty("id") private int id;
            @JsonProperty("datetime") private OffsetDateTime dateTime;
        }
    }
}
