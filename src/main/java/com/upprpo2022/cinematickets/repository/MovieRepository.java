package com.upprpo2022.cinematickets.repository;

import com.upprpo2022.cinematickets.domain.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieRepository extends JpaRepository<Movie, Integer> {

}
