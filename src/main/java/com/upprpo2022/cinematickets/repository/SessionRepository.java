package com.upprpo2022.cinematickets.repository;

import com.upprpo2022.cinematickets.domain.Session;
import com.upprpo2022.cinematickets.projections.MovieScheduleProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.OffsetDateTime;

public interface SessionRepository extends JpaRepository<Session, Integer> {
    @Query(value = "select s.id as id, s.dateTime as dateTime, s.theater.id as theaterId, s.theater.address as theaterAddress, s.theater.name as theaterName from Session s where s.movie.id = ?1 and s.dateTime between ?2 and ?3")
    Page<MovieScheduleProjection> findMovieScheduleByMovieIdAndDateTimeBetween(Integer movieId, OffsetDateTime dateTimeStart, OffsetDateTime dateTimeEnd, PageRequest pageRequest);
}
