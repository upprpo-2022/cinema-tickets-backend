package com.upprpo2022.cinematickets.service;

import com.upprpo2022.cinematickets.domain.Movie;
import com.upprpo2022.cinematickets.dto.MovieListDto;
import com.upprpo2022.cinematickets.repository.MovieRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class MovieService {
    private final MovieRepository movieRepository;

    public MovieService(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    public MovieListDto getMovieList(int pageNum) {
        Page<Movie> page = movieRepository.findAll(PageRequest.of(pageNum - 1, 50).withSort(Sort.by("id").ascending()));

        List<MovieListDto.MovieDto> movieDtoList = page.stream()
                .map(movie -> new MovieListDto.MovieDto(movie.getId(), movie.getName(), movie.getDescription(), movie.getPreviewUrl()))
                .toList();

        return new MovieListDto(page.getTotalPages(), pageNum, movieDtoList);
    }
}
