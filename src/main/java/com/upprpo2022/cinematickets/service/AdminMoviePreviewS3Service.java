package com.upprpo2022.cinematickets.service;

import com.upprpo2022.cinematickets.domain.Movie;
import com.upprpo2022.cinematickets.exception.ApiResponseStatusException;
import com.upprpo2022.cinematickets.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetUrlRequest;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

import java.io.IOException;
import java.security.SecureRandom;

import static com.upprpo2022.cinematickets.exception.ErrorString.BAD_REQUEST;

@Service
public class AdminMoviePreviewS3Service {
    private final String moviePreviewBucketName;
    private final S3Client s3Client;
    private final MovieRepository movieRepository;

    private final SecureRandom secureRandom = new SecureRandom();
    private static final String RANDOM_KEY_ALPHABET = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public AdminMoviePreviewS3Service(@Qualifier("moviePreviewBucketName") String moviePreviewBucketName,
                                      S3Client s3Client,
                                      MovieRepository movieRepository) {
        this.moviePreviewBucketName = moviePreviewBucketName;
        this.s3Client = s3Client;
        this.movieRepository = movieRepository;
    }

    public void uploadMoviePreview(Integer movieId, MultipartFile file) throws IOException {
        String objectKey;

        Movie movie = movieRepository.findById(movieId)
                .orElseThrow(() -> new ApiResponseStatusException(HttpStatus.BAD_REQUEST, BAD_REQUEST, "Movie id was not found"));

        if (movie.getPreviewObjectKey() != null) {
            objectKey = movie.getPreviewObjectKey();
        } else {
            objectKey = generateRandomKey();
        }

        PutObjectRequest putObjectRequest = PutObjectRequest.builder()
                .bucket(moviePreviewBucketName)
                .key(objectKey)
                .build();

        s3Client.putObject(putObjectRequest, RequestBody.fromBytes(file.getBytes()));

        if (movie.getPreviewObjectKey() == null) {
            String previewUrl = s3Client.utilities().getUrl(
                    GetUrlRequest.builder().bucket(moviePreviewBucketName).key(objectKey).build()).toExternalForm();

            movie.setPreviewObjectKey(objectKey);
            movie.setPreviewUrl(previewUrl);
            movieRepository.save(movie);
        }
    }

    private String generateRandomKey() {
        StringBuilder result = new StringBuilder();
        int length = 20;

        for (int i = 0; i < length; i++) {
            int n = secureRandom.nextInt(RANDOM_KEY_ALPHABET.length());
            result.append(RANDOM_KEY_ALPHABET.charAt(n));
        }
        return result.toString();
    }
}
