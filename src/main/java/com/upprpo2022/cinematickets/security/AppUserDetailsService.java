package com.upprpo2022.cinematickets.security;

import com.upprpo2022.cinematickets.domain.AppUser;
import com.upprpo2022.cinematickets.dto.ChangePasswordDto;
import com.upprpo2022.cinematickets.dto.RegistrationDto;
import com.upprpo2022.cinematickets.exception.ApiResponseStatusException;
import com.upprpo2022.cinematickets.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import static com.upprpo2022.cinematickets.exception.ErrorString.*;

@Service
public class AppUserDetailsService implements UserDetailsService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public AppUserDetailsService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AppUser appUser = userRepository.findByUsername(username);

        if (appUser == null) {
            throw new UsernameNotFoundException("Username not found");
        }

        return new AppUserDetails(appUser);
    }

    public void registerAppUser(RegistrationDto registrationDto) {
        if (userRepository.findByUsername(registrationDto.getUsername()) != null) {
            throw new ApiResponseStatusException(HttpStatus.BAD_REQUEST, USERNAME_TAKEN, "User already exists");
        }

        AppUser newAppUser = new AppUser();
        newAppUser.setUsername(registrationDto.getUsername());
        newAppUser.setPassword(passwordEncoder.encode(registrationDto.getPassword()));
        newAppUser.setRole("USER");

        userRepository.save(newAppUser);
    }

    public void changeAppUserPassword(ChangePasswordDto changePasswordDto) {
        AppUser appUser = userRepository.findByUsername(changePasswordDto.getUsername());

        if (userRepository.findByUsername(changePasswordDto.getUsername()) == null) {
            throw new ApiResponseStatusException(HttpStatus.UNAUTHORIZED, BAD_USERNAME, "User doesn't exist");
        }

        if (!passwordEncoder.matches(changePasswordDto.getOldPassword(), appUser.getPassword())) {
            throw new ApiResponseStatusException(HttpStatus.UNAUTHORIZED, BAD_PASSWORD, "Wrong password");
        }

        appUser.setPassword(passwordEncoder.encode(changePasswordDto.getNewPassword()));

        userRepository.save(appUser);
    }
}
