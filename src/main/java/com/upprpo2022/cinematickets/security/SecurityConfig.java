package com.upprpo2022.cinematickets.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final AppUserDetailsService appUserDetailsService;
    private final AuthenticationHandler authenticationHandler;
    private final PasswordEncoder passwordEncoder;

    public SecurityConfig(AppUserDetailsService appUserDetailsService, AuthenticationHandler authenticationHandler, PasswordEncoder passwordEncoder) {
        this.appUserDetailsService = appUserDetailsService;
        this.authenticationHandler = authenticationHandler;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(appUserDetailsService).passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests(customizer -> customizer
                        .antMatchers("/**").permitAll())
                .formLogin(configurer -> configurer
                        .successHandler(authenticationHandler)
                        .failureHandler(authenticationHandler))
                .csrf().disable()
                .cors();
    }
}
