package com.upprpo2022.cinematickets.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@ToString
@Entity(name = "Movie")
@Table(name = "movie")
public class Movie {
    @Id
    @Column(name = "id", columnDefinition = "SERIAL", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name", columnDefinition = "VARCHAR(50)", nullable = false)
    private String name;

    @Column(name = "description", columnDefinition = "VARCHAR(1000)", nullable = false)
    private String description;

    @Column(name = "preview_url", columnDefinition = "VARCHAR(150)")
    private String previewUrl;

    @Column(name = "preview_object_key", columnDefinition = "VARCHAR(50)")
    private String previewObjectKey;
}
