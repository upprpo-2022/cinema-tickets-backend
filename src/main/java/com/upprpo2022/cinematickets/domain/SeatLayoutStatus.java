package com.upprpo2022.cinematickets.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
public class SeatLayoutStatus {
    @JsonProperty("seats") private List<Seat> seats;

    @Getter
    @Setter
    public static class Seat {
        @JsonProperty("x") private Integer x;
        @JsonProperty("y") private Integer y;
        @JsonProperty("price") private BigDecimal price;
        @JsonProperty("status") private Status status;

        public enum Status {
            @JsonProperty("free") FREE,
            @JsonProperty("occupied") OCCUPIED
        }
    }
}
