package com.upprpo2022.cinematickets.exception;

import org.springframework.http.HttpStatus;

public class ApiResponseStatusException extends RuntimeException {
    private final HttpStatus httpStatus;
    private final String errorString;
    private final String errorMessage;

    public ApiResponseStatusException(HttpStatus httpStatus, ErrorString errorString, String errorMessage) {
        super();
        this.httpStatus = httpStatus;
        this.errorString = errorString.toString();
        this.errorMessage = errorMessage;
    }

    public ApiResponseStatusException(HttpStatus httpStatus, ErrorString errorString) {
        this(httpStatus, errorString, null);
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getErrorString() {
        return errorString;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
