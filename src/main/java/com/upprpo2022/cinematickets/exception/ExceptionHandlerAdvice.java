package com.upprpo2022.cinematickets.exception;

import com.upprpo2022.cinematickets.dto.ApiErrorDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandlerAdvice {
    @ExceptionHandler(ApiResponseStatusException.class)
    public ResponseEntity<ApiErrorDto> apiResponseStatusExceptionHandler(ApiResponseStatusException exception) {
        ApiErrorDto apiErrorDto = new ApiErrorDto();
        apiErrorDto.setErrorString(exception.getErrorString());
        apiErrorDto.setErrorMessage(exception.getErrorMessage());
        return new ResponseEntity<>(apiErrorDto, exception.getHttpStatus());
    }
}
