package com.upprpo2022.cinematickets.controller;

import com.upprpo2022.cinematickets.dto.ChangePasswordDto;
import com.upprpo2022.cinematickets.dto.RegistrationDto;
import com.upprpo2022.cinematickets.security.AppUserDetailsService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "",
        produces = MediaType.APPLICATION_JSON_VALUE)
public class AuthController {
    private final AppUserDetailsService appUserDetailsService;

    public AuthController(AppUserDetailsService appUserDetailsService) {
        this.appUserDetailsService = appUserDetailsService;
    }

    @PostMapping(value = "/register")
    public void register(@RequestBody RegistrationDto registrationDto) {
        appUserDetailsService.registerAppUser(registrationDto);
    }

    @PostMapping(value = "/change-password")
    public void changePassword(@RequestBody ChangePasswordDto changePasswordDto) {
        appUserDetailsService.changeAppUserPassword(changePasswordDto);
    }
}
