package com.upprpo2022.cinematickets.controller.admin;

import com.upprpo2022.cinematickets.domain.Movie;
import com.upprpo2022.cinematickets.dto.admin.AdminMovieDto;
import com.upprpo2022.cinematickets.dto.admin.mappers.AdminApiDtoMapper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/admin/movies")
public class MovieAdminApiController extends BaseAdminApiController<Movie, AdminMovieDto> {
    protected MovieAdminApiController(JpaRepository<Movie, Integer> repository, AdminApiDtoMapper<Movie, AdminMovieDto> adminApiDtoMapper) {
        super(repository, adminApiDtoMapper);
    }
}
