package com.upprpo2022.cinematickets.controller.admin;

import com.upprpo2022.cinematickets.domain.Theater;
import com.upprpo2022.cinematickets.dto.admin.AdminTheaterDto;
import com.upprpo2022.cinematickets.dto.admin.mappers.AdminApiDtoMapper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/admin/theaters")
public class TheaterAdminApiController extends BaseAdminApiController<Theater, AdminTheaterDto> {
    protected TheaterAdminApiController(JpaRepository<Theater, Integer> repository, AdminApiDtoMapper<Theater, AdminTheaterDto> adminApiDtoMapper) {
        super(repository, adminApiDtoMapper);
    }
}
