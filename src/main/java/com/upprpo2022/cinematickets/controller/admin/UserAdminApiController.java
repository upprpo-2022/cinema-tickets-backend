package com.upprpo2022.cinematickets.controller.admin;

import com.upprpo2022.cinematickets.domain.AppUser;
import com.upprpo2022.cinematickets.dto.admin.AdminUserDto;
import com.upprpo2022.cinematickets.dto.admin.mappers.AdminApiDtoMapper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/admin/users")
public class UserAdminApiController extends BaseAdminApiController<AppUser, AdminUserDto> {
    protected UserAdminApiController(JpaRepository<AppUser, Integer> repository, AdminApiDtoMapper<AppUser, AdminUserDto> adminApiDtoMapper) {
        super(repository, adminApiDtoMapper);
    }
}
