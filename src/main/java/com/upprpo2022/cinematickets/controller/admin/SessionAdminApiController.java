package com.upprpo2022.cinematickets.controller.admin;

import com.upprpo2022.cinematickets.domain.Session;
import com.upprpo2022.cinematickets.dto.admin.AdminSessionDto;
import com.upprpo2022.cinematickets.dto.admin.mappers.AdminApiDtoMapper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/admin/sessions")
public class SessionAdminApiController extends BaseAdminApiController<Session, AdminSessionDto> {
    protected SessionAdminApiController(JpaRepository<Session, Integer> repository, AdminApiDtoMapper<Session, AdminSessionDto> adminApiDtoMapper) {
        super(repository, adminApiDtoMapper);
    }
}
