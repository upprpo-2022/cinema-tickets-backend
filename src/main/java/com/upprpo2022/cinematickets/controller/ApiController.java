package com.upprpo2022.cinematickets.controller;

import com.upprpo2022.cinematickets.domain.Movie;
import com.upprpo2022.cinematickets.domain.SeatLayoutStatus;
import com.upprpo2022.cinematickets.domain.Session;
import com.upprpo2022.cinematickets.dto.MovieDto;
import com.upprpo2022.cinematickets.dto.MovieListDto;
import com.upprpo2022.cinematickets.dto.MovieScheduleDto;
import com.upprpo2022.cinematickets.dto.TicketOrderDto;
import com.upprpo2022.cinematickets.exception.ApiResponseStatusException;
import com.upprpo2022.cinematickets.repository.AuditoriumRepository;
import com.upprpo2022.cinematickets.repository.MovieRepository;
import com.upprpo2022.cinematickets.repository.SessionRepository;
import com.upprpo2022.cinematickets.repository.TheaterRepository;
import com.upprpo2022.cinematickets.service.MovieService;
import com.upprpo2022.cinematickets.service.SessionService;
import com.upprpo2022.cinematickets.service.TicketOrderService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.time.OffsetDateTime;

import static com.upprpo2022.cinematickets.exception.ErrorString.BAD_REQUEST;
import static com.upprpo2022.cinematickets.exception.ErrorString.NO_SUCH_ENTITY;

@RestController
@RequestMapping(value = "/api/v1/",
        produces = MediaType.APPLICATION_JSON_VALUE)
public class ApiController {
    private final MovieRepository movieRepository;
    private final TheaterRepository theaterRepository;
    private final AuditoriumRepository auditoriumRepository;
    private final SessionRepository sessionRepository;

    private final SessionService sessionService;
    private final TicketOrderService ticketOrderService;
    private final MovieService movieService;

    public ApiController(MovieRepository movieRepository,
                         TheaterRepository theaterRepository,
                         AuditoriumRepository auditoriumRepository,
                         SessionRepository sessionRepository,
                         SessionService sessionService,
                         TicketOrderService ticketOrderService, MovieService movieService) {
        this.movieRepository = movieRepository;
        this.theaterRepository = theaterRepository;
        this.auditoriumRepository = auditoriumRepository;
        this.sessionRepository = sessionRepository;
        this.sessionService = sessionService;
        this.ticketOrderService = ticketOrderService;
        this.movieService = movieService;
    }

    @GetMapping(value = "sessions/{id}/seats")
    public SeatLayoutStatus getSessionSeats(@PathVariable("id") int sessionId) {
        Session session = sessionRepository.findById(sessionId)
                .orElseThrow(() -> new ApiResponseStatusException(HttpStatus.BAD_REQUEST, BAD_REQUEST, "Session id was not found"));

        return session.getSeats();
    }

    @PostMapping(value = "ticket-order")
    public void postTicketOrder(@RequestBody TicketOrderDto ticketOrderDto) {
        ticketOrderService.handleTicketOrder(ticketOrderDto);
    }

    @GetMapping(value = "movie-schedule")
    public MovieScheduleDto getSessionsSchedule(@RequestParam("movie") int movieId,
                                                @RequestParam("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) OffsetDateTime startDate,
                                                @RequestParam("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) OffsetDateTime endDate,
                                                @RequestParam("page") int pageNum) {
        if (pageNum < 1) {
            throw new ApiResponseStatusException(HttpStatus.BAD_REQUEST, BAD_REQUEST, "Bad page number");
        }

        return sessionService.getMovieSchedule(movieId, startDate, endDate, pageNum);
    }

    @GetMapping(value = "movies")
    public MovieListDto getSessionsSchedule(@RequestParam("page") int pageNum) {
        if (pageNum < 1) {
            throw new ApiResponseStatusException(HttpStatus.BAD_REQUEST, BAD_REQUEST, "Bad page number");
        }

        return movieService.getMovieList(pageNum);
    }

    @GetMapping(value = "movie/{id}")
    public MovieDto getMovie(@PathVariable("id") int movieId) {
        Movie movie = movieRepository.findById(movieId).orElseThrow(() -> new ApiResponseStatusException(HttpStatus.BAD_REQUEST, NO_SUCH_ENTITY, "Movie id was not found"));
        return new MovieDto(movie.getId(), movie.getName(), movie.getDescription(), movie.getPreviewUrl());
    }
}
